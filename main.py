import sys
from rdflib import Graph, URIRef, Literal
from rdflib.namespace import FOAF, XSD, RDF, SKOS


def semantic_serializations():
    g = Graph()

    g.bind("foaf", FOAF)
    g.bind("xsd", XSD)
    g.bind("skos", SKOS)

    add_organization(g)
    add_person(g)


    print(g.serialize(format="turtle"))
    print("---------------------------------------------------------------")
    print(g.serialize(format="json-ld"))


def add_organization(g):
    g.add((
        URIRef("http://mondragon.edu/organization/MGEP"),
        RDF.type,
        FOAF.Organization
    ))

    g.add((
        URIRef("http://mondragon.edu/organization/MGEP"),
        SKOS.prefLabel,
        Literal("Mondragon goi eskola politeknikoa", datatype=XSD.string)
    ))


def add_person(g):
    g.add((
        URIRef("http://mondragon.edu/person/aperez"),
        RDF.type,
        FOAF.Person
    ))
    g.add((
        URIRef("http://mondragon.edu/person/aperez"),
        FOAF.givenName,
        Literal("Alain", datatype=XSD.string)
    ))
    g.add((
        URIRef("http://mondragon.edu/person/aperez"),
        FOAF.nick,
        Literal("aperez", datatype=XSD.string)
    ))

    g.add((
        URIRef("http://mondragon.edu/person/aperez"),
        FOAF.mbox,
        URIRef("mailto:aperez@mondraogn.edu")
    ))
    g.add((
        URIRef("http://mondragon.edu/person/aperez"),
        FOAF.member,
        URIRef("http://mondragon.edu/organization/MGEP"),
    ))


def query_dbpedia():
    g = Graph()

    g.parse('http://dbpedia.org/resource/Semantic_Web')

    for s, p, o in g:
        print(s, "\t\t=>\t\t", p, "\t\t=>\t\t", o)


def query_dbpedia_sparql():
    g = Graph()

    # example_query = """
    # SELECT DISTINCT ?Concept
    # WHERE {
    #   [] a ?Concept
    # }"""
    example_query = """
    SELECT DISTINCT ?pre ?obj
    WHERE {
    SERVICE <http://dbpedia.org/sparql> {
            <http://dbpedia.org/resource/Durango,_Biscay> ?pre ?obj .
        }
    }
    """

    qres = g.query(example_query)
    for row in qres:
        print("\t\t=>\t\t", row.pre, "\t\t=>\t\t", row.obj)


def main() -> int:
    semantic_serializations()

    #query_dbpedia()

    query_dbpedia_sparql()

    return 0


if __name__ == '__main__':
    sys.exit(main())